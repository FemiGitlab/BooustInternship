var a = "Buck";
var b = "Rabbit";
var c = "Deer";
var d = "Dog";
var e = "Gazelle";
var f = "Gibbon";
var g = "Gorilla";
var h = "Hippo";
var i = "Ibex";
var j = "Lion";
var k = "Monkey";
var l = "Panda";
var m = "Bear";
var n = "Puppy";
var o = "Rhino";
var p = "Squirrel";
var q = "Tiger";
var r = "Whale";
var s = "Wolf";
var t = "Elephant";

var animalArray = [a, b, c, d, e, f, g, h, i, j,
                   k, l, m, n, o, p, q, s, t];

//document.getElementById("image").style.visibility = "hidden"

/*var textInput = document.getElementById("text1").value;*/

function demoDisplay () {
    document.getElementById('image').style.visibility = "hidden";
    document.getElementById('text').style.visibility = "hidden";
    document.getElementById('randomText').style.visibility = "hidden";
}


function submitFunction () {
    document.getElementById('text').style.visibility = "visible";
    document.getElementById('randomText').style.visibility = "visible";

    var randomAnimal = animalArray[Math.floor(Math.random()*animalArray.length)];
    document.getElementById("randomText1").innerHTML=randomAnimal;
    /*document.getElementById("image").innerHTML = randomAnimal;*/

    var textInput = document.getElementById("text1").value;
    document.getElementById("enteredText").innerHTML = textInput;
    console.log (animalArray[8] + "  " + animalArray[2] + " " + randomAnimal);

    if (randomAnimal == textInput) {
        document.querySelector("#text").innerHTML = "You are right!!!";
        document.getElementById('image').style.visibility = "visible";

        if (randomAnimal==a) {
            document.querySelector("#image").src = './Buck-Logo.png';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal==b) {
            document.querySelector("#image").src = './Bunny-Rabbit-Logo.png';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal==c) {
            document.querySelector("#image").src = './Deer-Logo-Design.png';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal==d) {
            document.querySelector("#image").src = './Bull-Dog-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal==e) {
            document.querySelector("#image").src = './Gazelle-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal==f) {
            document.querySelector("#image").src = './Gibbon-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal==g) {
            document.querySelector("#image").src = './Gorilla-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal==h) {
            document.querySelector("#image").src = './Hippo-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid ##C4C4C4333333';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == i) {
            document.querySelector("#image").src = './Ibex-Goat-Logo.png';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == j) {
            document.querySelector("#image").src = './Lion-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == k) {
            document.querySelector("#image").src = './Monkey-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == l) {
            document.querySelector("#image").src = './Panda-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == m) {
            document.querySelector("#image").src = './Polar-Bear-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == n) {
            document.querySelector("#image").src = "./Puppy-Logo.jpg";
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == o) {
            document.querySelector("#image").src = './Rhino-Logo.png';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == p) {
            document.querySelector("#image").src = './Squirrel-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == q) {
            document.querySelector("#image").src = './Tigress-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == r) {
            document.querySelector("#image").src = './Whale-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else if (randomAnimal == s) {
            document.querySelector("#image").src = './Wolf-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
        else {
            document.querySelector("#image").src = './Yogge-Elephant-Logo.jpg';
            document.querySelector("#image").setAttribute("width", '280');
            document.querySelector("#image").setAttribute("height", '200');
            //document.querySelector("#image").style.border = '5px solid #C4C4C4';
            document.querySelector("#image").setAttribute("style", 'border-radius: 100px');
        }
    }
    else {
        document.querySelector("#text").innerHTML = "You got it wrong, try it again.";
    }

    document.getElementById("text1").value = "";
}

console.log (animalArray[8] + "  " + animalArray[2]);


function resetFunction() {
    //document.getElementById('image').innerHTML = "it works";
    document.getElementById('image').style.visibility = "hidden";
    document.getElementById('text').style.visibility = "hidden";
    document.getElementById('randomText').style.visibility = "hidden";
    document.getElementById("text1").value = "";
}